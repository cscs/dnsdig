# dnsdig

## Description
A little script using `dig` or `drill` to measure response times of DNS resolvers.

## Usage

##### Local

Save, mark executable, run.
```
curl -O https://gitlab.com/cscs/dnsdig/-/raw/main/dnsdig
```
```
chmod +x dnsdig
```
```
./dnsdig
```

##### Remote

The script can be used directly from this source.

```
bash <(curl -s https://gitlab.com/cscs/dnsdig/-/raw/main/dnsdig)
```

##### Options

A few extra options are avilable in the form of environment variables.

`DOMAIN=any.url` Can be used to change the domain used for lookups. <br>
`TESTDNS=X.Y.Y.Z` Can be used to test a resolver IP not already featured. <br>
`SKIP=1` Will skip the preliminary message as well as all queries except $CURRENTDNS and $TESTDNS if defined. <br>



## Featured DNS


| Provider     | Primary IP   | Secondary IP |
|--------------|--------------|--------------|
| <a href=https://adguard-dns.io/public-dns.html>AdGuard</a> | 94.140.14.14 | 94.140.15.15 |
| <a href=https://cleanbrowsing.org/filters>CleanBrowsing</a> | 185.228.168.9 | 185.228.169.9 |
| <a href=https://1.1.1.1>Cloudflare</a> | 1.1.1.1 | 1.0.0.1 |
| <a href=https://www.comodo.com/secure-dns>Comodo</a> | 8.26.56.26 | 8.20.247.20 |
| <a href=https://controld.com/free-dns>Control D</a> | 76.76.2.2 | 76.76.10.2 |
| <a href=https://www.dns0.eu>dns0.eu</a> | 193.110.81.0 | 185.253.5.0 |
| <a href=https://www.dnsfilter.com>DNSFilter</a> | 103.247.36.36 | 103.247.37.37 |
| <a href=https://docs.oracle.com/en-us/iaas/Content/DNS/Concepts/recursive-dns.htm>Dyn/Oracle</a> | 216.146.35.35 | 216.146.36.36 |
| <a href=https://flashstart.com/>FlashStart</a> | 185.236.104.104 | 185.236.105.105 |
| <a href=https://gcore.com/public-dns>Gcore</a> | 95.85.95.85 | 2.56.220.2 |
| <a href=https://developers.google.com/speed/public-dns>Google</a> | 8.8.8.8 | 8.8.4.4 |
| <a href=https://www.lumen.com>Level3/Lumen</a> | 209.244.0.3 | 209.244.0.4 |
| <a href=https://nextdns.io>NextDNS</a> | 45.90.28.105 | 45.90.30.105 |
| <a href=https://www.opendns.com>OpenDNS/Cisco</a> | 208.67.222.222 | 208.67.220.220 |
| <a href=https://www.quad9.net>Quad9</a> | 9.9.9.9 | 149.112.112.112 |
| <a href=https://www.safedns.com>SafeDNS</a> | 195.46.39.39 | 195.46.39.40 |
| <a href=https://vercara.com/ultra-dns-public>UltraDNS/Vercara</a> | 64.6.64.6 | 64.6.65.6 |

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)
